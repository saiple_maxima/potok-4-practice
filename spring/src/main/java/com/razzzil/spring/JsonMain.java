package com.razzzil.spring;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.razzzil.spring.dto.PersonDto;

public class JsonMain {
    public static void main(String[] args) throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();

        PersonDto personDto = PersonDto.builder()
                .lastName("Minneakhmetov")
                .firstName("Razil")
                .id(1)
                .build();

        String json = objectMapper.writeValueAsString(personDto);

        PersonDto fromJson = objectMapper.readValue(json, PersonDto.class);

        int i = 0;

    }
}
