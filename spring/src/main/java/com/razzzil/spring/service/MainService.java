package com.razzzil.spring.service;

import com.razzzil.spring.converter.PersonConverter;
import com.razzzil.spring.dto.PersonDto;
import com.razzzil.spring.dto.PersonSaveDto;
import com.razzzil.spring.model.Book;
import com.razzzil.spring.model.Person;
import com.razzzil.spring.repository.BookRepository;
import com.razzzil.spring.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MainService {

    private final PersonRepository personRepository;

    private final BookRepository bookRepository;

    private final PersonConverter personConverter;

    public void generate(){
        Person razil = Person.builder()
                .firstName("Razil")
                .lastName("Minneakhmetov")
                .build();

        personRepository.save(razil);

        Book book1 = Book.builder()
                .name("Book 1")
                .author(razil)
                .build();

        Book book2 = Book.builder()
                .name("Book 2")
                .author(razil)
                .build();

        bookRepository.save(book1);
        bookRepository.save(book2);
    }

    public List<Person> getAllPersons(){
        return personRepository.findAll();
    }


    public List<PersonDto> getAllPersonDtos(){
        List<Person> persons = personRepository.findAll();
        return personConverter.toPersonDtoList(persons);
    }

    public void save(PersonSaveDto personSaveDto){
        Person person = personConverter.toPerson(personSaveDto);
        personRepository.save(person);
    }

    public void update(PersonSaveDto personSaveDto, long id){
        Person person = personConverter.toPerson(personSaveDto);
        person.setId(id);
        personRepository.save(person);
    }
}
