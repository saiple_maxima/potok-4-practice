package com.razzzil.spring.service;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

@Service
public class TelegramBotService extends TelegramLongPollingBot {

    private final String botUsername;

    public TelegramBotService(@Value("${telegram.token}") String botToken,
                              @Value("${telegram.username}")String botUsername) {
        super(botToken);
        this.botUsername = botUsername;

    }

    @SneakyThrows
    @Override
    public void onUpdateReceived(Update update) {
        String message = update.getMessage().getText();
        Long chatId = update.getMessage().getChatId();

        String messageToSend;
        if (message.equals("Привет")) {
            messageToSend = "Здарова";
        } else if (message.equals("Как дела?")) {
            messageToSend = "Нормально";
        } else if (message.equals("Что делаешь?")) {
            messageToSend = "Пишу код";
        } else {
            messageToSend = "Не понял";
        }

        SendMessage sendMessage = new SendMessage(chatId.toString(), messageToSend);
        execute(sendMessage);
    }

    @Override
    public String getBotUsername() {
        return botUsername;
    }


}
