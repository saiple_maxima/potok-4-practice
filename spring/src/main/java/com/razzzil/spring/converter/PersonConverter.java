package com.razzzil.spring.converter;

import com.razzzil.spring.dto.PersonDto;
import com.razzzil.spring.dto.PersonSaveDto;
import com.razzzil.spring.model.Person;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PersonConverter {

    public List<PersonDto> toPersonDtoList(List<Person> persons) {
        return persons.stream()
                .map(this::toPersonDto)
                .collect(Collectors.toList());
    }

    public PersonDto toPersonDto(Person person) {
        return PersonDto.builder()
                .lastName(person.getLastName())
                .id(person.getId())
                .firstName(person.getFirstName())
                .build();
    }

    public Person toPerson(PersonSaveDto personSaveDto) {
        return Person.builder()
                .firstName(personSaveDto.getFirstName())
                .lastName(personSaveDto.getLastName())
                .build();
    }



}
