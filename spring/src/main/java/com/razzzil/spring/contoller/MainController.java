package com.razzzil.spring.contoller;

import com.razzzil.spring.dto.PersonDto;
import com.razzzil.spring.dto.PersonSaveDto;
import com.razzzil.spring.model.Person;
import com.razzzil.spring.service.MainService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/person")
public class MainController {

    private final MainService mainService;

    @GetMapping("/all")
    public List<PersonDto> all(){
        return mainService.getAllPersonDtos();
    }

    @PostMapping("/save")
    public void save(@RequestBody PersonSaveDto personDto){
        mainService.save(personDto);
    }

    @PostMapping("/save/{userId}")
    public void update(@PathVariable("userId") long userId,
                       @RequestBody PersonSaveDto personDto){
        mainService.update(personDto, userId);
    }

}
