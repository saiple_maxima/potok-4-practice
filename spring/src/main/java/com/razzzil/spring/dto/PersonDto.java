package com.razzzil.spring.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PersonDto {

    private long id;
   // @JsonProperty("LAST_NAME")
   // @JsonAlias({"lastName", "surname"})
    private String lastName;

    private String firstName;
}
