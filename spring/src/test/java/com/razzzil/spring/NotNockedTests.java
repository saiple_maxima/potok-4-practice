package com.razzzil.spring;

import com.razzzil.spring.model.Person;
import com.razzzil.spring.repository.PersonRepository;
import com.razzzil.spring.service.MainService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

@SpringBootTest
class NotNockedTests {

    @Autowired
    private MainService mainService;

    @Test
    void checkPersons(){
        //test
        List<Person> persons = mainService.getAllPersons();
        Person razil = null;
        for (Person person : persons) {
            if (person.getFirstName().equals("Razil") && person.getLastName().equals("Minneakhmetov")) {
                razil = person;
            }
        }
        //check
        Assertions.assertNotNull(razil);
    }

}
