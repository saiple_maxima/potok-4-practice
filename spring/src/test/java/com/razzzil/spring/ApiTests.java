package com.razzzil.spring;

import com.razzzil.spring.model.Person;
import com.razzzil.spring.repository.PersonRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ApiTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PersonRepository personRepository; // убрать если хотим лезть в базу

    @Test
    public void testApi() throws Exception {

        //mocking (optional)
        Person mockedPerson = Person.builder()
                .lastName("Minneakhmetov")
                .firstName("rererer")
                .id(1)
                .build();
        Mockito.when(personRepository.findAll()).thenReturn(List.of(mockedPerson));

        //test and check
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/person/all")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].firstName").value("Razil"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].lastName").value("Minneakhmetov"));
    }

}
