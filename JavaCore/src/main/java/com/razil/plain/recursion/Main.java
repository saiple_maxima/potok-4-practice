package com.razil.plain.recursion;

import java.util.Random;

public class Main {

    private static final Random random = new Random();

    public static void main(String[] args) {
        test(7);
    }

    public static void test(int i){
        System.out.println("Передано: " + i);
        int next = random.nextInt(10);
        if (i != 5) {
            System.out.println(i + " != 5. Идем дальше");
            test(next);
        } else {
            System.out.println("Все");
        }
    }

}
