package com.razil.plain.annotation;


import javax.annotation.processing.Generated;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
public @interface CustomAnnotation {

    String value();

    String whatToDo() default "nfsjkdfnk";

}
