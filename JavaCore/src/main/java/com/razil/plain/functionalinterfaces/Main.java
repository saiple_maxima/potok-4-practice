package com.razil.plain.functionalinterfaces;

import java.util.function.*;

public class Main {

    @FunctionalInterface
    public interface TripleFunction<T, U, V, R> {

        /**
         * Applies this function to the given arguments.
         *
         * @param t the first function argument
         * @param u the second function argument
         * @return the function result
         */
        R apply(T t, U u, V v);
    }


    public static void main(String[] args) {
        doSmth(s -> s + s + s);

        doSmth((s, s2) -> s + s2);

        doSmth((s, s2, s3) -> s + s2 + s3);

        doSmth(s -> {
            System.out.println(s);
        });

        doSmth((s, s2) -> {
            System.out.println(s + s2);
        });

        doSmth(() -> "Привет");

    }

    public static void doSmth(Function<String, String> function) {
        String returnValue = function.apply("Привет");
        System.out.println(returnValue);
    }

    public static void doSmth(BiFunction<String, String, String> function) {
        String returnValue = function.apply("Привет", "Как дела");
        System.out.println(returnValue);
    }

    public static void doSmth(TripleFunction<String, String, String, String> function) {
        String returnValue = function.apply("Привет", "Как дела?", "Что делаешь?");
        System.out.println(returnValue);
    }

    public static void doSmth(Consumer<String> consumer) {
        consumer.accept("Привет");
    }

    public static void doSmth(BiConsumer<String, String> consumer) {
        consumer.accept("Привет", "Как дела");
    }

    public static void doSmth(Supplier<String> supplier) {
        String returnValue = supplier.get();
        System.out.println(returnValue);
    }

    public class ReturnFormLambda {
        private int a;
        private int b;
    }
}
