package com.razil.plain.lambda;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {

        PostProcess helloWorld = () -> {
            System.out.println("Hello world");
            return "njvs;vjsjv";
        };

        PostProcess currentDate = new PostProcess() {
            @Override
            public String postProcess() {
                LocalDate date = LocalDate.now();
                System.out.println("Current date: " + date);
                return date.toString();
            }
        };

        startPostProcess(helloWorld);
        startPostProcess(currentDate);

    }


    public static void startPostProcess(PostProcess postProcess){
        System.out.println("Start process...");
        String string = postProcess.postProcess();
        System.out.println("Return value from postProcess: " + string);
        System.out.println("End process");
    }
}
