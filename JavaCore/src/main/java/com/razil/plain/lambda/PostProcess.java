package com.razil.plain.lambda;

public interface PostProcess {

    String postProcess();

}
