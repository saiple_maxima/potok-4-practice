package com.razil.plain.ternary;

import java.time.LocalDate;

public class TernaryOperator {
    public static void main(String[] args) {
        int currentYear = LocalDate.now().getYear();
        String howToSay;

        if (currentYear == 2023) {
            howToSay = "УРА НОВЫЙ ГОД!";
        } else {
            howToSay = "НЕ НОВЫЙ ГОД :(";
        }

        String howToSay2 = currentYear == 2023 ? "УРА НОВЫЙ ГОД!" : "НЕ НОВЫЙ ГОД :(";
    }
}
