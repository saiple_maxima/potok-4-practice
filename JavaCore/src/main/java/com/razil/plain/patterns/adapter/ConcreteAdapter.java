package com.razil.plain.patterns.adapter;

public class ConcreteAdapter implements Adapter {

    private Adaptee adaptee = new Adaptee();

    @Override
    public void operation() {
        adaptee.greeting();
    }
}
