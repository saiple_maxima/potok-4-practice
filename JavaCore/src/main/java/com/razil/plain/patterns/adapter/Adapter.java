package com.razil.plain.patterns.adapter;

public interface Adapter {

    void operation();

}
