package com.razil.plain.patterns.adapter;

public class Client {

    public static final Adapter adapter = new ConcreteAdapter();

    public static void main(String[] args) {
        adapter.operation();
    }
}
