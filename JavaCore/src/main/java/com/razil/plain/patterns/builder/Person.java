package com.razil.plain.patterns.builder;

public class Person {

    private String name;
    private String firstName;
    private String lastName;
    private String gender;
    private int age;

    public Person(String name,
                  String firstName,
                  String lastName,
                  String gender,
                  int age) {
        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
    }

    public Person() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public static PersonBuilder builder() {
        return new PersonBuilder(new Person());
    }

    public static class PersonBuilder {

        private Person person;

        public PersonBuilder(Person person) {
            this.person = person;
        }

        public PersonBuilder name(String name){
            person.setName(name);
            return this;
        }

        public PersonBuilder firstName(String firstName){
            person.setFirstName(firstName);
            return this;
        }

        public PersonBuilder lastName(String lastName){
            person.setName(lastName);
            return this;
        }

        public PersonBuilder gender(String gender){
            person.setGender(gender);
            return this;
        }

        public PersonBuilder age(int age){
            person.setAge(age);
            return this;
        }

        public Person build(){
            return person;
        }
    }

}
