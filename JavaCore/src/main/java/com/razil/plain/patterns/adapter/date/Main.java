package com.razil.plain.patterns.adapter.date;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

        List<String> strings = new ArrayList<>();

        strings.add("klfsdnlfksdf");
        strings.add("klfsfwrwerdnlfksdf");
        strings.add("klfwrwerwersdnlfksdf");
        strings.add("twtwerwerwerwer");

        Comparator<String> comparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return new Random().nextInt(2) - 1;
            }
        };

        strings.sort(comparator);

        System.out.println(strings);
    }
}
