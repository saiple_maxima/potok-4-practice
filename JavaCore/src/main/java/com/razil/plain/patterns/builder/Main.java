package com.razil.plain.patterns.builder;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person();
        person1.setAge(23);
        person1.setLastName("Minneakhmetov");
        person1.setName("Razil");
        person1.setFirstName("Razil");

        Person person2 = new Person("Razil", "Razil", "Minneakhmetov", "Male", 23);

        Person person3 = Person.builder()
                .age(23)
                .lastName("LastNAme")
                .name("NAme")
                .firstName("dknvlskvd")
                .build();


        Person.PersonBuilder personBuilder = Person.builder();

    }
}
