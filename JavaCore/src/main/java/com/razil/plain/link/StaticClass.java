package com.razil.plain.link;

import java.util.Arrays;
import java.util.Collections;

public class StaticClass {

    public static int substraction1(int[] array) {
        int index = 0;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < array.length; i++) {
            if (max < array[i]) {
                max = array[i];
                index = i;
            }
        }
        for (int i = 0; i < array.length; i++) {
            if (i != index) {
                max -= array[i];
            }
        }
        return max;
    }

    public static int substraction2(int[] array) {
        int[] sortedArray = Arrays.copyOf(array, array.length);
        Arrays.sort(sortedArray);
        int maxValue = sortedArray[sortedArray.length - 1];
        for (int i = sortedArray.length - 2; i >= 0 ; i--) {
            maxValue -= sortedArray[i];
        }
        return maxValue;
    }

}
