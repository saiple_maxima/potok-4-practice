package com.razil.plain.objectequals;

import java.util.Objects;

public class Person {
    private String name;
    private String surname;
    private String passportData;
    private int age;

    private Person parent;

    public Person(String name,
                  String surname,
                  String passportData,
                  int age) {
        this.name = name;
        this.surname = surname;
        this.passportData = passportData;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Person)) {
            return false;
        }
        Person person = (Person) o;
        return Objects.equals(passportData, person.passportData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, passportData, age, parent);
    }
}
