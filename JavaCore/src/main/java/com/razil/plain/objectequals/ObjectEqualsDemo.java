package com.razil.plain.objectequals;

public class ObjectEqualsDemo {
    public static void main(String[] args) {

        Person person = new Person("Razil", "Minneakhmetov", "935-28-502", 23);
        Person razil = person;
        boolean equality = razil.equals("kndsv;kls");
        System.out.println(equality);
    }
}
