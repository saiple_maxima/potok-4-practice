package com.razil.plain.generics;

public class SenderEmail implements Sender<ReturnTextEmail> {

    @Override
    public ReturnTextEmail send(String destination,
                                String text) {


        //логика по отправке email
        return new ReturnTextEmail(text, destination);
    }
}
