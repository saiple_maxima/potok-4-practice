package com.razil.plain.generics;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public abstract class ReturnText {

    private String text;
    private String destination;

    public abstract SendType getType();

    @Override
    public String toString() {
        return "ReturnText{" +
                "text='" + text + '\'' +
                ", destination='" + destination + '\'' +
                ", type='" + getType() + '\'' +
                '}';
    }
}
