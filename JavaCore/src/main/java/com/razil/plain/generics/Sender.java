package com.razil.plain.generics;

public interface Sender<T extends ReturnText> {

    T send(String destination, String text);

}
