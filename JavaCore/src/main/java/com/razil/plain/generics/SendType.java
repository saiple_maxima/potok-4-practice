package com.razil.plain.generics;

public enum SendType {

    EMAIL, SMS;

}
