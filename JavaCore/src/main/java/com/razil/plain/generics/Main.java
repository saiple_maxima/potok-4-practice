package com.razil.plain.generics;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Введите тип отправки");
        Scanner scanner = new Scanner(System.in);
        String sendTypeString = scanner.nextLine();
        SendType sendType = SendType.valueOf(sendTypeString);

        Sender<?> sender;
        switch (sendType) {
            case EMAIL -> sender = new SenderEmail();
            case SMS -> sender = new SenderSms();
            default -> throw new IllegalArgumentException("SendType "  + sendType + " is not supported");
        }
        ReturnText returnText = sender.send("Destination", "Text");
        System.out.println(returnText);

    }
}
