package com.razil.plain.generics;


public class ReturnTextSms extends ReturnText {

    public ReturnTextSms(String text,
                         String destination) {
        super(text, destination);
    }

    @Override
    public SendType getType() {
        return SendType.SMS;
    }
}
