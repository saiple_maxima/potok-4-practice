package com.razil.plain.generics;

public class SenderSms implements Sender<ReturnTextSms> {

    @Override
    public ReturnTextSms send(String destination, String text) {
        //логика по отправке sms
        return new ReturnTextSms(text, destination);
    }
}
