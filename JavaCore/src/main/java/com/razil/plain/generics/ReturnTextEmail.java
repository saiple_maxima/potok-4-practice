package com.razil.plain.generics;

public class ReturnTextEmail extends ReturnText {

    public ReturnTextEmail(String text,
                           String destination) {
        super(text, destination);
    }

    @Override
    public SendType getType() {
        return SendType.EMAIL;
    }
}
