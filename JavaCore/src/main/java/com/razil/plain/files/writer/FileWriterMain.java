package com.razil.plain.files.writer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.*;
import java.util.Scanner;

import static com.razil.plain.files.reader.FileReaderMain.FILE_URL;
import static com.razil.plain.files.reader.FileReaderMain.TEMP_FILE_URL;

public class FileWriterMain {
    public static void main(String[] args) throws IOException {

        char zero = '0';

        System.out.println();


    }

    private static void writeFileWithFileWriter() throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(FILE_URL, StandardCharsets.UTF_8, true))) {
            // запись всей строки
         //   String text = "Hello Gold!";

            writer.write(48);
            writer.write("48");
//            // запись по символам
//            writer.append('\n');
//            writer.append('E');
            writer.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static void deleteUser(int id) throws IOException {
        File tempfile = new File(TEMP_FILE_URL);
        File file = new File(FILE_URL);
        tempfile.createNewFile();
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(TEMP_FILE_URL, true))) {
            try (Scanner scanner = new Scanner(new File(FILE_URL))) {
                while (scanner.hasNext()) {
                    String line = scanner.nextLine();
                    String[] params = line.split("|");
                    int currectUserId = Integer.parseInt(params[0]);
                    if (currectUserId != id) {
                        writer.write(line + "\n");
                    }
                }
            }
            writer.flush();
            file.delete();
            tempfile.renameTo(file);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
