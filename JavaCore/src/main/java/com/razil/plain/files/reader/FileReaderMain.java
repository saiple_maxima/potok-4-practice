package com.razil.plain.files.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.*;
import java.util.Scanner;

public class FileReaderMain {

    public static final String FILE_PATH_URL = "/Users/saiple/Documents/Maxima/potok-4-practice/src/com/razil/files/reader";
    public static final String FILE_URL = FILE_PATH_URL + "/file.txt";
    public static final String TEMP_FILE_URL = FILE_PATH_URL + "/tempfile.txt";

    public static void main(String[] args) throws IOException {
        readFromFileWithScanner();
      //  readFromFileWithBufferedReader();
    }

    private static void readFromFileWithScanner() throws IOException {
        try (Scanner scanner = new Scanner(new File(FILE_URL), StandardCharsets.UTF_8)) {
            while (scanner.hasNext()){
                String line  = scanner.nextLine();
                System.out.println(line);
            }
        }
    }

    private static void readFromFileWithBufferedReader() {
        try (BufferedReader reader = new BufferedReader(new FileReader(FILE_URL, StandardCharsets.UTF_8))) {
            int c;
            while ( (c=reader.read() ) !=-1 ) {
                System.out.print((char)c);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
