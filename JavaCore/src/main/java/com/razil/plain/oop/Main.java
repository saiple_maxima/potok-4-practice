package com.razil.plain.oop;

import java.util.Random;

public class Main {

    private static final Random RANDOM = new Random();

    public static void main(String[] args) {
        Circle circle = new Circle(10);
        Ellipse ellipse = new Ellipse(5, 10);

        Rectangle rectangle = new Rectangle(3, 6);
        Square square = new Square(4);

        Figure[] figures = {circle, ellipse, rectangle, square};

        for (int i = 0; i < figures.length; i++) {
            Figure figure = figures[i];
            System.out.println("Тип фигуры: " + figure.getFigureType());
            System.out.println("Периметр: " + figure.getPerimeter());
        }

        System.out.println("Moveable: ");

        Moveable[] moveables = {circle, square};

        for (Moveable moveable : moveables) {
            System.out.println("Тип фигуры: " + moveable.getFigureType());
            int x = RANDOM.nextInt(Constants.MAX_RANDOM_VALUE);
            int y = RANDOM.nextInt(Constants.MAX_RANDOM_VALUE);
            System.out.println("Было в X: " + moveable.getX());
            System.out.println("Было в Y: " + moveable.getY());
            moveable.move(x, y);
            System.out.println("Пемещено в X: " + moveable.getX());
            System.out.println("Пемещено в Y: " + moveable.getY());
        }

    }

}
