package com.razil.plain.oop;

public abstract class Figure implements Typeable {

    public abstract int getPerimeter();

}
