package com.razil.plain.oop;

import java.util.Random;

import static com.razil.plain.oop.Constants.MAX_RANDOM_VALUE;

public class Square extends Rectangle implements Moveable {

    private int centerX;
    private int centerY;

    public Square(int a) {
        super(a, a);
        Random random = new Random();
        centerX = random.nextInt(MAX_RANDOM_VALUE);
        centerY = random.nextInt(MAX_RANDOM_VALUE);
    }

    @Override
    public int getPerimeter() {
        return 4 * getA();
    }

    @Override
    public String getFigureType() {
        return "Квадрат";
    }

    @Override
    public void move(int x, int y) {
        centerX += x;
        centerY += y;
    }

    @Override
    public int getX() {
        return centerX;
    }

    @Override
    public int getY() {
        return centerY;
    }
}
