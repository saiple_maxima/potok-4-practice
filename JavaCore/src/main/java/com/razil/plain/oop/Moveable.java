package com.razil.plain.oop;

public interface Moveable extends Typeable {

    void move(int x, int y);

    int getX();

    int getY();
}
