package com.razil.plain.oop;

import java.util.Random;

import static com.razil.plain.oop.Constants.MAX_RANDOM_VALUE;

public class Circle extends Ellipse implements Moveable {

    private int centerX;
    private int centerY;

    public Circle(int r1) {
        super(r1, r1);
        Random random = new Random();
        centerX = random.nextInt(MAX_RANDOM_VALUE);
        centerY = random.nextInt(MAX_RANDOM_VALUE);
    }

    @Override
    public int getPerimeter() {
        return (int) Math.round(2 * Math.PI * getR1());
    }

    @Override
    public String getFigureType() {
        return "Круг";
    }

    @Override
    public void move(int x, int y) {
        centerX += x;
        centerY += y;
    }

    @Override
    public int getX() {
        return centerX;
    }

    @Override
    public int getY() {
        return centerY;
    }

}
