package com.razil.plain.oop;

public class Rectangle extends Figure {

    private int a;
    private int b;

    public Rectangle(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public int getPerimeter() {
        return 2 * (a + b);
    }

    @Override
    public String getFigureType() {
        return "Прямоугольник";
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }
}
