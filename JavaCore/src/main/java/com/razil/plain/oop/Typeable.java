package com.razil.plain.oop;

public interface Typeable {
    String getFigureType();
}
