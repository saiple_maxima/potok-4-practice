package com.razil.plain.oop;

public class Ellipse extends Figure {

    private int r1;
    private int r2;

    public Ellipse(int r1, int r2) {
        this.r1 = r1;
        this.r2 = r2;
    }

    @Override
    public int getPerimeter() {
        return (int) Math.round(4 * (Math.PI * r1 * r2 + (r1 - r2)) / (r1 + r2)) ;
    }

    @Override
    public String getFigureType() {
        return "Эллипс";
    }

    public int getR1() {
        return r1;
    }

    public void setR1(int r1) {
        this.r1 = r1;
    }

    public int getR2() {
        return r2;
    }

    public void setR2(int r2) {
        this.r2 = r2;
    }
}
