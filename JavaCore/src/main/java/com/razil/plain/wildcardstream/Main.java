package com.razil.plain.wildcardstream;

import java.util.*;
import java.util.stream.*;

public class Main {

    public static void main(String[] args) {
        Man man = new Man("Razil", "Minneakhmetov");
        Woman woman = new Woman("Katya", "Ivanova");
        Boy boy = new Boy("Ivan", "Ivanov");
        Girl girl = new Girl("Vika", "Petrova");
        Dog dog = new Dog("Rex");
      //  Person person = new Person("Person", "person");

        List<? extends Person> personList = List.of(man, woman, boy, girl);

        List<String> persons = personList.stream()
                .filter(person -> person instanceof Woman)
                .map(Person::getFirstName)
                .sorted(Comparator.comparingInt(String::length))
                .collect(Collectors.toList());

        System.out.println(persons);

        int i = 0;

    }

    public static class Mammal {

    }


    public static class Person extends Mammal {
        protected String firstName;
        protected String lastName;

        public Person(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    '}';
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }
    }

    public static class Man extends Person {

        public Man(String firstName,
                   String lastName) {
            super(firstName, lastName);
        }
    }

    public static class Woman extends Person {

        public Woman(String firstName,
                     String lastName) {
            super(firstName, lastName);
        }
    }

    public static class Boy extends Man {

        public Boy(String firstName,
                   String lastName) {
            super(firstName, lastName);
        }
    }

    public static class Girl extends Woman {

        public Girl(String firstName,
                    String lastName) {
            super(firstName, lastName);
        }
    }

    public static class Dog extends Mammal {
        private String name;

        public Dog(String name) {
            this.name = name;
        }
    }

}
