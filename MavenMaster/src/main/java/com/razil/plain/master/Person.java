package com.razil.plain.master;

import com.razil.plain.slave.Passport;
import com.razil.slave.*;
import lombok.Data;

@Data
public class Person {
    private String firstName;
    private String lastName;
    private Passport passport;

}
