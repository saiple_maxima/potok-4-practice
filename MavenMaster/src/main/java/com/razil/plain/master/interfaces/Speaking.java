package com.razil.plain.master.interfaces;

public interface Speaking extends Cloneable {

    void greeting();

    void bye();

}
