package com.razil.plain.master;

import com.razil.plain.master.interfaces.Cat;
import com.razil.plain.master.interfaces.Dog;
import com.razil.plain.master.interfaces.Speaking;
import com.razil.slave.*;

public class Main {
    public static void main(String[] args) {

        int c = UtilClass.sum(1, 2);

        System.out.println(c);


//        List<Speaking> speaking = List.of(new Cat(), new Dog());
//
//        speaking.forEach(Speaking::bye);
//        speaking.forEach(Speaking::greeting);
//        speaking.forEach(Speaking::bye);
//        speaking.forEach(Speaking::greeting);
//        speaking.forEach(Speaking::bye);
//        speaking.forEach(Speaking::greeting);
//        speaking.forEach(Speaking::bye);
//
//        speaking.forEach(Speaking::greeting);
//        speaking.forEach(Speaking::bye);
//        speaking.forEach(Speaking::greeting);
//        speaking.forEach(Speaking::bye);
//        speaking.forEach(Speaking::greeting);
//        speaking.forEach(Speaking::bye);
//        speaking.forEach(Speaking::greeting);

        Speaking speaking = new Cat();

        speaking.bye();
        speaking.greeting();
        speaking.bye();
        speaking.greeting();

        speaking = new Dog();

        speaking.bye();
        speaking.greeting();
        speaking.bye();
        speaking.greeting();


    }
}
