package com.razil.plain.tdd;

import com.razil.plain.master.tdd.Repository;
import com.razil.plain.master.tdd.RepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RepositoryTest {

    Repository repository = new RepositoryImpl();

    @Test
    public void test(){
        String currentAge = repository.currentAge();
        Assertions.assertEquals("2023", currentAge);
    }

}
