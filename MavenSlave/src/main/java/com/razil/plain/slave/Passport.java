package com.razil.plain.slave;

import lombok.Data;

import java.util.*;
@Data
public class Passport {

    private String number;
    public Date dueDate;

}
