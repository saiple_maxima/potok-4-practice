package com.razil.plain.slave;

import lombok.Data;

@Data
public class Address {
    public String street;
    public int house;
    public int apartment;
}
