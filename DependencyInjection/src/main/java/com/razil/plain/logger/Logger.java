package com.razil.plain.logger;

public interface Logger {

    void info(String message);
    void error(String message);
    void debug(String message);
}
