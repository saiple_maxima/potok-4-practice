package com.razil.plain.logger;

public enum LoggingLevel {
    INFO, ERROR, DEBUG
}
