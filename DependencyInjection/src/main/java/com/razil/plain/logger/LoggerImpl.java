package com.razil.plain.logger;

import com.razil.plain.repository.DateRepository;
import com.razil.plain.repository.LoggingLevelRepository;

public class LoggerImpl implements Logger {

    public final DateRepository dateRepository;
    public final LoggingLevelRepository loggingLevelRepository;

    public LoggerImpl(DateRepository dateRepository,
                      LoggingLevelRepository loggingLevelRepository) {
        this.dateRepository = dateRepository;
        this.loggingLevelRepository = loggingLevelRepository;
    }


    private void log(LoggingLevel loggingLevel, String message) {
        System.out.println(loggingLevel + " Date: " + dateRepository.currentDate() + "; Message: "  + message);
    }

    @Override
    public void info(String message) {
        if (loggingLevelRepository.currentLoggingLevel() == LoggingLevel.INFO) {
            log(LoggingLevel.INFO, message);
        }
    }

    @Override
    public void error(String message) {
        if (loggingLevelRepository.currentLoggingLevel() == LoggingLevel.ERROR) {
            log(LoggingLevel.ERROR, message);
        }
    }

    @Override
    public void debug(String message) {
        if (loggingLevelRepository.currentLoggingLevel() == LoggingLevel.DEBUG) {
            log(LoggingLevel.DEBUG, message);
        }
    }
}
