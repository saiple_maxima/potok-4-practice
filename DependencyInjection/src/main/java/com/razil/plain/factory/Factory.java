package com.razil.plain.factory;

import com.razil.plain.logger.Logger;
import com.razil.plain.logger.LoggerImpl;
import com.razil.plain.repository.DateRepository;
import com.razil.plain.repository.impl.DateRepositoryImpl;
import com.razil.plain.repository.LoggingLevelRepository;
import com.razil.plain.repository.impl.LoggingLevelRepositoryImpl;

public class Factory {

    public static Logger constructLogger(){
        DateRepository dateRepository = new DateRepositoryImpl();
        LoggingLevelRepository loggingLevelRepository = new LoggingLevelRepositoryImpl();
        return new LoggerImpl(dateRepository, loggingLevelRepository);
    }

}
