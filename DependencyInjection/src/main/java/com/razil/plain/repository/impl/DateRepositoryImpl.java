package com.razil.plain.repository.impl;

import com.razil.plain.repository.DateRepository;

import java.util.Date;

public class DateRepositoryImpl implements DateRepository {
    @Override
    public Date currentDate() {
        return new Date();
    }
}
