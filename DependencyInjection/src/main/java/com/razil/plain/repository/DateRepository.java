package com.razil.plain.repository;

import java.util.Date;

public interface DateRepository {

    Date currentDate();

}
