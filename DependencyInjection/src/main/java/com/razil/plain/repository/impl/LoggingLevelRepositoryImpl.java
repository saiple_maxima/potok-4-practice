package com.razil.plain.repository.impl;

import com.razil.plain.logger.LoggingLevel;
import com.razil.plain.repository.LoggingLevelRepository;

public class LoggingLevelRepositoryImpl implements LoggingLevelRepository {
    @Override
    public LoggingLevel currentLoggingLevel() {
        return LoggingLevel.INFO;
    }
}
