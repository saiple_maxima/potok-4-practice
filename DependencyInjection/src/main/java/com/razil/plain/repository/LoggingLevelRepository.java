package com.razil.plain.repository;

import com.razil.plain.logger.LoggingLevel;

public interface LoggingLevelRepository {

    LoggingLevel currentLoggingLevel();
}
