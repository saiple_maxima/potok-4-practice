package com.razil.spring.repository;

import java.util.Date;

public interface DateRepository {

    Date currentDate();

}
