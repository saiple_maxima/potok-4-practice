package com.razil.spring.repository;

import com.razil.spring.logger.LoggingLevel;

public interface LoggingLevelRepository {

    LoggingLevel currentLoggingLevel();
}
