package com.razil.spring.repository.impl;

import com.razil.spring.repository.DateRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Date;

@Component
public class DateRepositoryImpl implements DateRepository {
    @Override
    public Date currentDate() {
        return new Date();
    }
}
