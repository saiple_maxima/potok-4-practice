package com.razil.spring.repository.impl;

import com.razil.spring.logger.LoggingLevel;
import com.razil.spring.repository.LoggingLevelRepository;
import org.springframework.stereotype.Component;

@Component
public class LoggingLevelRepositoryImpl implements LoggingLevelRepository {
    @Override
    public LoggingLevel currentLoggingLevel() {
        return LoggingLevel.INFO;
    }
}
