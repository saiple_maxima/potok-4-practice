package com.razil.spring.service;

import com.razil.plain.factory.Factory;
import com.razil.spring.logger.Logger;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Scope("prototype")
public class SimpleServiceOne {

    private final Logger logger;


    public void textSmth(){
        logger.info("smth");
    }

}
