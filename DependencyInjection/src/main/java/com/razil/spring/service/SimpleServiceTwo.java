package com.razil.spring.service;

import com.razil.spring.logger.Logger;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SimpleServiceTwo {

    private final Logger logger;


    public void textSmth(){
        logger.info("smth");
    }

}
