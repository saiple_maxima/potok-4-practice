package com.razil.spring;

import com.razil.spring.config.AppConfig;
import com.razil.spring.logger.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);

        Logger logger = applicationContext.getBean("loggerImpl", Logger.class);

        int i = 0;
    }
}
