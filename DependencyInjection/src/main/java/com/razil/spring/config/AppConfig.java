package com.razil.spring.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

@Configuration
@ComponentScan("com.razil.spring")
public class AppConfig {

    @Bean
    public Date date(){
        return new Date();
    }
}
