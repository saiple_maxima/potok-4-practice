package com.razil.spring.logger;

public interface Logger {

    void info(String message);
    void error(String message);
    void debug(String message);
}
