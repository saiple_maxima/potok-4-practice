package com.razil.spring.logger;

public enum LoggingLevel {
    INFO, ERROR, DEBUG
}
